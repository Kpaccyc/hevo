; Used to set parameters for filament such as retracts, temperatures etc
M291 P"loading config.g for ABS/PA GF" R"Loading config" S1 T2
M302 S250 R150                ; set cold extrude and retract temperatures
set global.BedPreheatTemp=100 ; set be preheat variable
M572 D0 S0.03                 ; set pressure advance
M207 S0.5 F1500 Z0.2          ; define 0.5mm retraction, 0.2mm Z hop