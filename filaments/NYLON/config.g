; Used to set parameters for filament such as retracts, temperatures etc
M291 P"loading config.g for NYLON" R"Loading config" S1 T2
set global.BedPreheatTemp=70 ; set be preheat variable
M302 S230 R130               ; set cold extrude and retract temperatures