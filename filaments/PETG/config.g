; Used to set parameters for filament such as retracts, temperatures etc
M291 P"loading config.g for PETG" R"Loading config" S1 T2
M302 S200 R100               ; set cold extrude and retract temperatures
set global.BedPreheatTemp=70 ; set be preheat variable
M572 D0 S0.06                ; define pressure advance
M207 S0.5 F4800 Z0.2         ; define 0.5mm retraction, 0.2mm Z hop