; Used to set parameters for filament such as retracts, temperatures etc
M291 P"loading config.g for PLA" R"Loading config" S1 T2
M302 S185 R90                ; set cold extrude and retract temperatures
set global.BedPreheatTemp=60 ; set be preheat variable
M207 S4 F3600 Z0             ; Set retraction