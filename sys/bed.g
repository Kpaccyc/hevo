var minDeviation = 0.02 ; Set the minimum acceptable devation

G29 S2                  ; turn off bed mesh levelling
M561                    ; clear any bed transform
M290 R0 S0              ; clear babystepping

; If the printer hasn't been homed, home it
if !move.axes[0].homed || !move.axes[1].homed || !move.axes[2].homed
	G28
	if result != 0
		abort "Print cancelled due to homing error"

while true
  if iterations = 5
    abort "Too many auto calibration attempts"
                        ;probe at XMin mesh probe point and midway of Y mesh area 
  G30 P0 X10 Y52 Z-99999
  if result != 0
    abort "Print cancelled due to probe error"
                        ;probe at X Max mesh probe point and midway of Y mesh area
  G30 P1 X140 Y243 Z-99999
  if result != 0
    abort "Print cancelled due to probe error"
                        ;probe at X Max mesh probe point and midway of Y mesh area
  G30 P2 X270 Y52 Z-99999 S3
  if result != 0
    abort "Print cancelled due to probe error"
                        ;set maximum probe deviation allowed between the two points
  if (abs(move.calibration.initial.deviation) <= var.minDeviation)
    break
                        ; if deviation between the two points is too high, repeat the test after Z axis has been adjusted. 
  echo "Repeating calibration because deviation is too high " ^ move.calibration.initial.deviation ^ "mm"
; end loop
G28 Z                   ; home z again incase levelling has affected z height