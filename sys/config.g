;#################### General preferences ###############
G90                                                         ; send absolute coordinates...
M83                                                         ; ...but relative extruder moves
M550 P"HEVO"                                                ; set printer name
M669 K1                                                     ; select CoreXY mode

;#################### Network ###########################
M551 P"ghj100gfhjkm"                                        ; set password
M552 S1                                                     ; enable network
M586 P0 S1                                                  ; enable HTTP
M586 P1 S0                                                  ; disable FTP
M586 P2 S0                                                  ; disable Telnet

;#################### Axis Mapping ######################
M584 X0 Y1 Z2:4:3 E5                                        ; set drive mapping

;#################### Drives ############################
M569 P0 S1                                                  ; physical drive 0 goes forwards using default driver timings
M569 P1 S1                                                  ; physical drive 1 goes forwards using default driver timings
;M569 P0 S1 D3 V30
;M569 P1 S1 D3 V30
M569 P2 S1                                                  ; physical drive 2 goes forwards using default driver timings
M569 P3 S1                                                  ; physical drive 3 goes forwards using default driver timings
M569 P4 S1                                                  ; physical drive 4 goes forwards using default driver timings
M569 P5 S1                                                  ; orbiter extruder physical drive 5 goes forwards using default driver timings
M350 X16 Y16 Z16 E16 I1                                     ; configure microstepping with interpolation
M92 X80 Y80 Z400                                            ; set steps per mm (80 for x16 160 for x32 microsteps)
M92 E690                                                    ; orbiter extruder steps per mm
M566 X900 Y900 Z60 E300  P1                                 ; maximum instantaneous speed changes (mm/min) (jerk)
M203 X30000 Y30000 Z600 E4800                               ; maximum speeds (mm/min)
M201 X10000 Y10000 Z200 E5000                               ; accelerations
;M204 P20000 T20000                                          ; max print and travel speed
M906 X1750 Y1750 Z1100 I30                                  ; set motor currents (mA) and motor idle factor in per cent
M906 E1100                                                  ; set extruder motor current
M84 S30                                                     ; Set idle timeout

;#################### Axis Limits #######################
M208 X0 Y0 Z0 S1                                            ; set axis minima
M208 X280 Y305 Z265 S0                                      ; set axis maxima
M671 X-45:140:325 Y-10:315:-10 S20                          ; set gantry pivot points

;#################### Endstops ##########################
M574 X1 S3                                                  ; configure sensorless endstop for low end on X
M574 Y2 S3                                                  ; configure sensorless endstop for high end on X
M915 X Y R0 F0 S10                                          ; configure stall detection
M574 Z1 S2                                                  ; configure Z-probe endstop for low end on Z

;#################### Print Head Probe ##################
M950 S0 C"servo0"                                           ; create servo pin 0 for BLTouch
M558 P9 C"^probe" H8:2 F600:90 T6000 A10 S0.02 R0.5         ; set Z probe type to bltouch and the dive height + speeds
M557 X15:260 Y15:260 S20                                    ; define mesh grid
G31 K0 P25 X0.0 Y41.57 Z1.88                                ; Z probe parameters


;#################### Heaters ###########################
M308 S0 P"bedtemp" Y"thermistor" T100000 B4725 C7.060000e-8 ; configure sensor 0 as thermistor on pin bedtemp
M950 H0 C"bed" T0                                           ; create bed heater output on bed and map it to sensor 0
M307 H0 B0 S1.00                                            ; disable bang-bang mode for the bed heater and set PWM limit
M140 H0                                                     ; map heated bed to heater 0
M143 H0 S130                                                ; set temperature limit for heater 0 to 130C

;M308 S1 P"e0temp" Y"thermistor" T100000 B5000 C1.587780e-7  ; configure sensor 1 as thermistor on pin e0temp
M308 S1 P"e0temp" Y"thermistor" T100000 B4138 C0 H0 L0
M950 H1 C"e0heat" T1                                        ; create nozzle heater output on e0heat and map it to sensor 1
M307 H1 B0 S1.00                                            ; disable bang-bang mode for heater  and set PWM limit
M143 H1 S350                                                ; set temperature limit for heater 1 to 300C

M308 S2 P"e2temp" Y"thermistor" T100000 B3950 C0 H0 L0      ; configure sensor 2 as chamber thermistor
M950 H2 C"e2heat" T2                                        ; create chamber heater output on bed and map it to sensor 0
M307 H2 B0 S1.00                                            ; disable bang-bang mode for the bed heater and set PWM limit
M141 H2
M143 H2 S70                                                 ; set temperature limit for heater 2 to 70C
;#################### Fans ##############################
M950 F0 C"fan1" Q100                                        ; create fan 0 on pin fan1 and set its frequency
M106 P0 S0 H1 T30:50 C"Hotend Fan"                          ; set fan 0 value. Thermostatic control is turned on
M950 F1 C"fan0" Q100                                        ; create fan 1 on pin fan0 and set its frequency
M106 P1 S0 H-1                                              ; set fan 1 value. Thermostatic control is turned off


;#################### Tools #############################
M563 P0 S"TZ2v6" D0 H1 F1                                   ; define tool 0
G10 P0 X0 Y0 Z0                                             ; set tool 0 axis offsets
G10 P0 R0 S0                                                ; set initial tool 0 active and standby temperatures to 0C

;#################### Filament Sensor ###################
;M591 D0 P7 C"zmax" L1.18 R70:130 S0 E6                      ; configure filament monitoring for tool0

;#################### Pressure Advance ##################
;M572 D0 S0.1                                                ; define pressure advance for D0

;##################### Input Shaping ####################
M593 P"mzv" F36                                             ; define input shaping

;#################### Firmware Retraction ###############
M207 S0.5 F4800 Z0.2                                        ; define 0.5mm retraction, 0.2mm Z hop

;#################### 12864 Screen ######################
;M98 P"screen.g"                                         ; run screen.g

;############ Print after a power failure ###############
;M911 S24.6 R25 P"M568 P0 A0 M913 X0 Y0 G91 M83"

;#################### Load Settings from Config-override 
M501                                                        ; load overrides from config-override.g

;#################### Globals ###########################
M98 P{directories.system ^ "globals.g"}                     ; initialize global variables

T0                                                          ; select tool 0