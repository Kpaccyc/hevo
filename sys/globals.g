if !exists(global.RunDamon)                                     ; checks for existence of global.RanDaemon
	global RunDaemon = true                                        ; if it doesn't exist, set the value to true
if !exists(global.slicerBedTemp)                                ; checks for the existence of global.slicerBedTemp
	global slicerBedTemp = 0                                       ; if it doesn't exist, set the value to 0
if !exists(global.slicerBedTempOverride)                        ; checks for the existence of global.slicerBedTempOverride
	global slicerBedTempOverride = 0                               ; if it doesn't exist, set the value to 0
if !exists(global.slicerHotendTemp)                             ; checks for the existence of global.slicerHotendTemp
	global slicerHotendTemp = 0                                    ; if it doesn't exist, set the value to 0
if !exists(global.slicerHotendTempOverride)                     ; checks for the existence of global.slicerHotendTempOverride
	global slicerHotendTempOverride = 0                            ; if it doesn't exist, set the value to 0
if !exists(global.soakTime)                                     ; checks for the existence of global.soakTime
	global soakTime = 15                                           ; if it doesn't exist, set the value in seconds. Value is in minutes
if !exists(global.soakTimeOverride)                             ; checks for the existence of global.soakTimeOverride
	global soakTimeOverride = false                                ; if it doesn't exist, set the value to false
if !exists(global.chamberCheckOverride)                         ; checks for the existence of global.chamberCheckOverride
	global chamberCheckOverride = false                            ; if it doesn't exist, set the value to false
if !exists(global.overrideBedOff)                               ; checks for the existence of global.overrideBedOff
	global overrideBedOff = false                                  ; if it doesn't exist, set the value to false
if !exists(global.overrideHotendOff)                            ; checks for the existence of global.overrideHotendOff
	global overrideHotendOff = false                               ; if it doesn't exist, set the value to false
if !exists(global.nozzleDiameterInstalled)                      ; checks for the existence of global.nozzleDiameterInstalled
	global nozzleDiameterInstalled = 0.4                           ; if it doesn't exist, set the value to 0.4mm, which is the default for the Troodon V2
if !exists(global.nozzleProbeTemperature)                       ; checks for the existence of global.nozzleProbeTemperature
	global nozzleProbeTemperature = 175                            ; if it doesn't exist, set the value to 175 degrees
if !exists(global.Cancelled)                                    ; checks for the existence of global.Cancelled
	global Cancelled = false                                       ; if it doesn't exist, set the value to false
if !exists(global.Bed_Center_X)                                 ; checks for existence of global.bed_center_x
	global Bed_Center_X = floor(move.axes[0].max / 2)              ; if it doesn't exist, calculate bed center x
if !exists(global.Bed_Center_Y)                                 ; checks for existence of global.bed_center_y
	global Bed_Center_Y = floor(move.axes[1].max / 2)              ; if it doesn't exist, calculate bed center y
if !exists(global.BedHasSoaked)                                 ; checks for existence of BedHasSoaked
	global BedHasSoaked = false                                    ; if it doesn't exist, set the value to false
if !exists(global.filamentRetractSpeed)                         ;checks for existence of filamentRetractSpeed
	global filamentRetractSpeed = 300                              ; if it doesn't exist, set the value to 300
if !exists(global.pausePurgeActive)                             ; checks for existence of pausePurgeActive. Used to determine if filament should be purged duriing pauses.
	global pausePurgeActive = false                                ; if it doesn't exist, set valuet to false
if !exists(global.pausePurgeAmount)                             ; checks for existence of pausePurgeAmount
	global pausePurgeAmount = 0.5                                  ; if it doesn't exist, set value to 0.5
if !exists(global.LoadedFilament) || global.LoadedFilament=null ; check for existance of LoadedFilament
	global LoadedFilament="No_Filament"                            ; if it doesn't exist, set value to No_Filament
if !exists(global.BedPreheatTemp)                               ; checks for existence of BedPreheatTemp
	global BedPreheatTemp=0                                        ; if it doesn't exist, set value to 0
if !exists(global.filamentDistance)                             ; checks for existence of filamentDistance
	global filamentDistance = 0                                    ; if it doesn't exist, set value to 0
if !exists(global.filamentFeedSpeed)                            ; checks for existance of filamentFeedSpeed
	global filamentFeedSpeed = 600                                 ; if it doesn't exist, set value to 600
if !exists(global.runtime)                                      ; checks for existance of runtime
	M98 P{directories.system ^ "runtime.g"}                        ; if it doesn't exist, read file