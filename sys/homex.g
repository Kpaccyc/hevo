M400           ; Wait for current moves to finish
M569 P0 S1 D3 V40
M569 P1 S1 D3 V40
M913 X100 Y100 ; drop motor current to 75%
M400
G91
G1 H2 Z5 F800
;if sensors.endstops[0].triggered = true
;	echo "End stop triggered.  Moving away"
;	M564 S0 H0
;	G1 X20 F1200 
;	M564 H1 S1
;	M400
;	if sensors.endstops[0].triggered = true
;		abort "X Endstop appears to be faulty.  Still in triggered state."
G1 H1 X{-(move.axes[0].max+50)} F4800
if result != 0
	abort "Error duing fast homing X axis - process cancelled"
G1 X5 F1200
G1 H1 X{-(move.axes[0].max+50)} F3600
if result != 0
	abort "Error duing slow homing X axis - process cancelled"
G1 H2 Z-5 F800
G90
M400
M913 X100 Y100 ; return current to 100%
M569 P0 S1 D2
M569 P1 S1 D2
M400