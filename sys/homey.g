M400           ; Wait for current moves to finish
M569 P0 S1 D3 V40
M569 P1 S1 D3 V40
M913 X75 Y75   ; drop motor current to 70%
M400
G91            ; relative positioning
G1 H2 Z5 F800
;if sensors.endstops[1].triggered = true ; if we're hard against the Y endstop we need to move away
;	M564 H0 S0
;	G1 Y-20 F1200
;	M564 H1 S1
;	M400
;	if sensors.endstops[1].triggered = true
;		abort "X Endstop appears to be faulty.  Still in triggered state."	
G1 H1 Y{(move.axes[1].max+50)} F4800
if result != 0
	abort "Error duing fast homing Y axis- process cancelled"
G1 Y-5 F1200
G1 H1 Y{(move.axes[1].max+50)} F3600
if result != 0
	abort "Error duing slow homing Y axis - process cancelled"
G1 H2 Z-5 F800
G90
M400
M913 X100 Y100 ; return current to 100%
M569 P0 S1 D2
M569 P1 S1 D2
M400