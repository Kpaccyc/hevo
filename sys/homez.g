var minDeviation = 0.018                                                                                               ; Set the minimum acceptable devation
if !move.axes[0].homed || !move.axes[1].homed                                                                          ; If the printer hasn't been homed, home it
	G28 XY                                                                                                                ; home y and x
G91                                                                                                                    ; relative positioning
G1 H2 Z5 F800                                                                                                          ; lift Z relative to current position
G90                                                                                                                    ; absolute positioning
G1 X{(move.axes[0].max)/2 - sensors.probes[0].offsets[0]} Y{(move.axes[1].max)/2 - sensors.probes[0].offsets[1]} F1200 ; Move to the centre of the bed taking the zprobe offsets into account
;M558 A1 F300                                                                                                           ; Set single probing at faster feed rate
G30                                                                                                                    ; probe the bed
;M558 A10 F100                                                                                                          ; Set detailed probing at slower feed rate
;G30                                                                                                                    ; Probe again to get a more accurate position