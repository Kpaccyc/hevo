;set global.Cancelled = false
;if sensors.filamentMonitors[0].status="ok"
;	set global.filamentDistance = 0 ; reset filament sensor extrusion distance after tripping
;T R1                             ; select last tool used
;M144 S1                          ; restore bed to active temp
;M568 P{state.currentTool} A2     ; set nozzle in active
;M106 R1                          ; restore part cooling fan speed to what it was at pause
;M116                             ; wait for temps
;if global.pausePurgeActive=true && global.pausePurgeAmount > 0
;	M291 R"Clean nozzle" P"Wipe purged filament from nozzle" S3 T60
G1 R1 X0 Y0 Z5 F3600             ; go to 5mm above position of the last print move
G1 R1 X0 Y0 Z0                   ; go back to the last print move
M83                              ; relative extruder moves
G1 E10 F3600                     ; extrude 10mm of filament