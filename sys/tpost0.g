M591 D0 S0               ; enable filament sensor
if state.nextTool != -1
	if heat.heaters[tools[state.nextTool].heaters[0]].active > 0
		M116 P{state.nextTool} ; wait for temps
		echo "waiting for tool", state.nextTool, "temp"
M703